import time

from behave import *
from pageobjects.bookingwidget import BOOKINGWIDGET


@given("user is on booking widget page")
def step_impl(context):
    var = BOOKINGWIDGET(context).go_to()
    assert var is not None, "The user is not navigated to Booking Widget screen."


@when('user clicks on "{appoint_btn}", the user is moved to "{booking_screen}" screen.')
def step_impl(context, appoint_btn, booking_screen):
    BOOKINGWIDGET(context).click_on(appoint_btn)
    booking_title = BOOKINGWIDGET(context).locator_dictionary["make_a_booking_title"]
    assert BOOKINGWIDGET(context).is_element_displayed(
        booking_title), "The user is not navigated to " + booking_screen + " screen. "


@when('user clicks on "{button}"')
def step_impl(context, button):
    BOOKINGWIDGET(context).click_on(button)


@step('user clicks "{location_dropdown}" and selects "{location}".')
def step_impl(context, location_dropdown, location):
    BOOKINGWIDGET(context).click_on(location_dropdown)
    time.sleep(2)
    BOOKINGWIDGET(context).click_on(location)


@step('user selects "{category}" and "{service}".')
def step_impl(context, category, service):
    BOOKINGWIDGET(context).click_on(category)
    BOOKINGWIDGET(context).click_on(service)


@when('user clicks on "{next_btn}" button, the user is moved to "{preferred_staff}" screen.')
def step_impl(context, next_btn, preferred_staff):
    BOOKINGWIDGET(context).click_on(next_btn)
    staff_title = BOOKINGWIDGET(context).locator_dictionary["preferred_staff_title"]
    get_title_locator = BOOKINGWIDGET(context).find_element(BOOKINGWIDGET(context).locator_dictionary["title_of_page"])
    if BOOKINGWIDGET(context).get_element_text(get_title_locator) == "Select Date & Time":
        pass
    else:
        assert BOOKINGWIDGET(context).is_element_displayed(
            staff_title), "The user is not navigated to " + preferred_staff + " screen."


@step('user selects Preferred "{staff}", the user is moved to "{date_time_screen}" screen.')
def step_impl(context, staff, date_time_screen):
    BOOKINGWIDGET(context).select_preferred_staff(staff)
    date_time_screen_title = BOOKINGWIDGET(context).locator_dictionary["date_time_screen_title"]
    assert BOOKINGWIDGET(context).is_element_displayed(
        date_time_screen_title), "The user is not navigated to " + date_time_screen + " screen."


@step('user selects the "{date}" and "{time_slot}"')
def step_impl(context, date, time_slot):
    BOOKINGWIDGET(context).select_date_time(date, time_slot)


@when('user clicks on "{next_btn}" button, the user is moved to the "{appointment_screen}" screen.')
def step_impl(context, next_btn, appointment_screen):
    BOOKINGWIDGET(context).click_on(next_btn)
    appointment_info_title = BOOKINGWIDGET(context).locator_dictionary["appointment_info_title"]
    assert BOOKINGWIDGET(context).is_element_displayed(
        appointment_info_title), "The user is not navigated to " + appointment_screen + " screen."


@then('validate the appointments names "{service}"')
def step_impl(context, service):
    # BOOKINGWIDGET(context).get_element_text(service)
    pass


@when('user clicks on "{next_btn}" then the user is moved to "{personal_details_screen}" screen.')
def step_impl(context, next_btn, personal_details_screen):
    BOOKINGWIDGET(context).click_on(next_btn)
    personal_details_screen_title_text = BOOKINGWIDGET(context).locator_dictionary["personal_details_screen_title"]
    assert BOOKINGWIDGET(context).is_element_displayed(
        personal_details_screen_title_text), "The user is not navigated to " + personal_details_screen + " screen."


@step(
    'user fills personal details as: "{first_name}", "{last_name}", "{email}", "{country_code}", '
    '"{mobile_phone_number}", "{comments}", and enable "{is_agreed}" and "{is_consented}" checkboxes')
def step_impl(context, first_name, last_name, email, country_code, mobile_phone_number, comments, is_agreed,
              is_consented):
    BOOKINGWIDGET(context).sign_up_form(first_name, last_name, email, country_code, mobile_phone_number, comments,
                                        is_agreed,
                                        is_consented)


@when('user clicks on "{confirm_booking_btn}", the user is navigated to "{prepayment_screen}" screen.')
def step_impl(context, confirm_booking_btn, prepayment_screen):
    BOOKINGWIDGET(context).click_on(confirm_booking_btn)
    prepayment_screen_title = BOOKINGWIDGET(context).locator_dictionary["prepayment_screen_title"]
    assert BOOKINGWIDGET(context).is_element_displayed(
        prepayment_screen_title), "The user is not navigated to " + prepayment_screen + " screen."


@step('validate "{service_value}" and "{deposit_amount}"')
def step_impl(context, service_value, deposit_amount):
    pass


@when('user clicks on the "{payment_btn}", the user will be moved to "{payment_screen}" screen.')
def step_impl(context, payment_btn, payment_screen):
    BOOKINGWIDGET(context).clicks_on_make_payment(payment_btn)
    payment_screen_title = BOOKINGWIDGET(context).locator_dictionary["payment_screen_title"]
    assert BOOKINGWIDGET(context).is_element_displayed(
        payment_screen_title), "The user is not navigated to " + payment_screen + " screen."


@step(
    'user fills credit card details as: Card number = "{credit_card}", Expiry date = "{card_expiry}", CVC = "{cvc}", '
    'ZIP = "{zip_code}" and hit "{pay_button}" button.')
def step_impl(context, credit_card, card_expiry, cvc, zip_code, pay_button):
    BOOKINGWIDGET(context).enter_payment_details(credit_card, card_expiry, cvc, zip_code, pay_button)
    success_title = BOOKINGWIDGET(context).locator_dictionary["success_title"]
    assert BOOKINGWIDGET(context).is_element_displayed(
        success_title), "The user is not navigated to Success screen."


@step(
    'the user validate the appointments names "{services}" again and note '
    'down the booking number')
def step_impl(context, services):
    pass


@when('user clicks on "{close_button}" button.')
def step_impl(context, close_button):
    BOOKINGWIDGET(context).success_and_close_booking(close_button)
    welcome_title = BOOKINGWIDGET(context).locator_dictionary["welcome_title"]
    assert BOOKINGWIDGET(context).is_element_displayed(
        welcome_title), "The user is not navigated to Welcome screen."
