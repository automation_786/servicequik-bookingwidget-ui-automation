Feature: Booking widget

  @smoke_test
  Scenario: Test booking through booking widget
    Given user is on booking widget page
    When user clicks on "Make an Appointment", the user is moved to "Make a booking" screen.
    And user clicks "location dropdown" and selects "location".
    And user selects "category" and "service".
    When user clicks on "Next " button, the user is moved to "preferred staff" screen.
    And user selects Preferred "staff", the user is moved to "Select Date & Time" screen.
    And user selects the "date" and "time"
    When user clicks on "Next " button, the user is moved to the "Appointment info" screen.
    Then validate the appointments names "i.e. Between Brows - LADIES IPL"
    When user clicks on "Go to Next Step" then the user is moved to "Personal details" screen.
    And user fills personal details as: "QA", "Tester", "qa@gmail.com", "+65", "97500441", "By Automated user", and enable "I agree " and "I consent " checkboxes
    When user clicks on "Confirm Booking", the user is navigated to "Prepayment" screen.
    And validate "Service Value" and "Deposit Amount"
    When user clicks on the "Make Payment", the user will be moved to "Payment" screen.
    And user fills credit card details as: Card number = "4242424242424242", Expiry date = "1222", CVC = "222", ZIP = "42424" and hit "Pay" button.
    And the user validate the appointments names "Between Brows - LADIES IPL" again and note down the booking number
    When user clicks on "Close" button.