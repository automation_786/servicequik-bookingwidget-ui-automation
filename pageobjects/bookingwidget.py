from selenium.webdriver.support.wait import WebDriverWait

from .common.basepage import BASEPAGE
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from utils.general import *
import time


class BOOKINGWIDGET(BASEPAGE):
    locator_dictionary = {
        "title_of_page": (By.CLASS_NAME, 'title'),
        "welcome_title": (By.XPATH, '//div[text() = "Welcome!"]'),
        "make_a_booking_title": (By.XPATH, '//div[text() = "Make a booking"]'),
        "make_an_appointment_btn": (By.XPATH, './/button[text() = "Make an Appointment"]'),
        "select_location_dropdown": (By.ID, 'select-location'),
        "select_first_location": (By.XPATH, './/ng-dropdown-panel/div/div[2]/div/span[text() = "Shenton Way"]'),

        "select_category": (By.XPATH, './/app-service-tree/div/div[1]/div/div[1]'),
        "select_service": (By.XPATH, './/app-service-tree/div/div[1]/app-radio-button-group/div/div['
                                     '1]/app-checkbox/label/span'),
        "next_btn": (By.XPATH, './/button[text() = "Next "]'),
        "preferred_staff_title": (By.XPATH, './/div[text() = "Preferred Staff"]'),
        "next_btn_staff": (By.XPATH, './/app-select-staff/button'),
        "select_staff": (By.XPATH, './/app-select-staff/main/div[1]/span'),
        "date_time_back_btn": (By.XPATH, './/app-wizard-header/span'),
        "date_time_screen_title": (By.XPATH, './/div[text() = "Select Date & Time"]'),
        "select_date": (By.XPATH, './/td[@tabindex="0"][contains(@class, "myDpCurrMonth")]'),
        "next_month_button": (By.XPATH, './/app-select-date/form/lib-angular-mydatepicker-calendar/div/div/lib'
                                        '-selection-bar/div/div[3]/button'),
        "select_time": (By.XPATH, './/app-select-time/div[2]/div[1]/div[1]'),
        "appointment_info_title": (By.XPATH, './/div[text() = "Appointment info"]'),
        "go_to_next_step_btn": (By.XPATH, './/app-appointment-list/button'),
        "personal_details_screen_title": (By.XPATH, './/div[text() = "Personal details"]'),

        "first_name": (By.ID, 'first-name'),
        "last_name": (By.ID, 'last-name'),
        "email_address": (By.ID, 'emailAddress'),
        "country_code_input": (By.XPATH, '//*[@id="mobilePhoneNumber"]/div/div/div[1]/ng-select/div/div/div[3]/input'),
        "country_code": (By.XPATH, './/span[text() = "Singapore"]'),
        "mobile_phone_number": (By.XPATH, './/input[@placeholder = "Mobile phone number"]'),
        "comments": (By.ID, 'comments'),

        "i_agree": (By.XPATH, './/app-checkbox/label[text() = "I agree "]/span'),
        "i_consent": (By.XPATH, './/app-checkbox/label[text() = "I consent "]/span'),

        "confirm_booking_btn": (By.XPATH, './/button[text() = "Confirm Booking "]'),
        "prepayment_screen_title": (By.XPATH, './/div[text() = "Prepayment"]'),

        "make_payment_btn": (By.XPATH, './/footer/button[text() = "Make Payment "]'),
        "payment_screen_title": (By.XPATH, './/div[text() = "Payment"]'),
        "pay_later_btn": (By.XPATH, './/button[text() = "Pay later "]'),
        "close_btn": (By.XPATH, './/button[text() = "Close"]'),

        "iframe_payment": (By.XPATH, './/*[contains(@name, "__privateStripeFrame")]'),
        "credit_card": (By.XPATH, './/span[@class="InputContainer"]//input[@placeholder="Card number"]'),
        "card_expiry": (By.XPATH, './/span[@class="InputContainer"]//input[@placeholder="MM / YY"]'),
        "cvc": (By.XPATH, './/span[@class="InputContainer"]//input[@placeholder="CVC"]'),
        "zip_code": (By.XPATH, './/span[@class="InputContainer"]//input[@placeholder="ZIP"]'),

        "pay_btn": (By.ID, 'submit'),
        "success_title": (By.XPATH, './/h2[contains(text(), "Reservation was created")]')
    }

    clickable_elements = {
        "Make an Appointment": "make_an_appointment_btn",
        "location dropdown": "select_location_dropdown",
        "location": "select_first_location",
        "category": "select_category",
        "service": "select_service",
        "Next ": "next_btn",
        "Next": "next_btn_staff",
        "Go to Next Step": "go_to_next_step_btn",
        "staff": "select_staff",
        "date": "select_date",
        "time": "select_time",
        "I agree ": "i_agree",
        "I consent ": "i_consent",
        "Confirm Booking": "confirm_booking_btn",
        "Make Payment": "make_payment_btn",
        "Pay Later": "pay_later_btn",
        "Pay": "pay_btn",
        "Close": "close_btn"
    }
    constants = {
        "link": '/bookings/'
    }

    def go_to(self):
        base_url = get_setting("URL", "url")
        self.browser.get(base_url + self.constants["link"])
        return self.find_element(self.locator_dictionary["welcome_title"])

    """def click_on_make_an_appointment(self):
        self.click_element(
            self.find_element(self.locator_dictionary["make_an_appointment_btn"])
        )"""

    def click_on(self, element):
        self.click_element(
            self.find_element(self.locator_dictionary[self.clickable_elements[element]])
        )

    def sign_up_form(self, first_name, last_name, email, country_code, mobile_phone_number, comments, is_agreed, is_consented):
        self.send_text_to_element(self.find_element((self.locator_dictionary["first_name"])), first_name)
        self.send_text_to_element(self.find_element((self.locator_dictionary["last_name"])), last_name)
        self.send_text_to_element(self.find_element((self.locator_dictionary["email_address"])), email)
        """self.send_text_to_element(self.find_element((self.locator_dictionary["country_code_input"])), country_code)
        
        self.click_element(self.locator_dictionary["country_code"])"""
        self.send_text_to_element(self.find_element((self.locator_dictionary["mobile_phone_number"])),
                                  mobile_phone_number)
        self.send_text_to_element(self.find_element((self.locator_dictionary["comments"])), comments)

        self.click_on(is_agreed)
        self.click_on(is_consented)

    def enter_payment_details(self, credit_card, card_expiry, cvc, zip_code, pay_button):
        WebDriverWait(self.browser, self.WAIT).until(
            EC.frame_to_be_available_and_switch_to_it(self.locator_dictionary["iframe_payment"]))

        # if self.is_element_displayed(self.locator_dictionary["iframe_payment"]):
        # self.click_element(self.find_element(self.locator_dictionary["credit_card"]))
        self.send_text_to_element(self.find_element((self.locator_dictionary["credit_card"])), credit_card)
        self.send_text_to_element(self.find_element((self.locator_dictionary["card_expiry"])), card_expiry)
        self.send_text_to_element(self.find_element((self.locator_dictionary["cvc"])), cvc)
        self.send_text_to_element(self.find_element((self.locator_dictionary["zip_code"])), zip_code)

        time.sleep(5)
        self.browser.switch_to.default_content()
        self.click_on(pay_button)

    def clicks_on_make_payment(self, payment_btn):
        time.sleep(5)
        self.click_element(
            self.find_element(self.locator_dictionary[self.clickable_elements[payment_btn]])
        )
        """if self.is_element_displayed(self.locator_dictionary["iframe_payment"]):
            pass
        else:
            self.clicks_on_make_payment(payment_btn)"""

    def success_and_close_booking(self, close_button):
        time.sleep(5)
        self.click_on(close_button)
        time.sleep(5)

    def select_date_time(self, date, time_slot):
        if self.find_element(self.locator_dictionary["select_date"]) is None:
            self.click_element(self.find_element(self.locator_dictionary["next_month_button"]))
        else:
            self.click_on(date)
            self.click_on(time_slot)

    def select_preferred_staff(self, staff):
        if self.find_element(self.locator_dictionary["select_staff"]) is None:
            pass
        else:
            self.click_on(staff)
            self.click_on("Next")
